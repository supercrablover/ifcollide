#include <iostream>
double readAmountOfAngles(){
double N{};//amount of angles in a figure
std::cin >> N;
return N;
}
struct Point{
double x;
double y;
};
Point readPointCoordinates(){
Point dot{};
std::cin >> dot.x;
std::cin >> dot.y;
return dot;
}
bool ifCollide(Point dot, int x1, int y1, int x2, int y2){
bool answer{};
double k = (static_cast<double>(y2-y1)/(x2-x1));
double m1 = (y1-k*x1);
double m2 = (y2 - k*x2);
double x = (dot.y - m1 )/k;
double y = k*x + m1;
 if ((x1<x<x2) && (y1<y<y2))
    answer = true;
 else
    answer = false;
return answer;
}
int main(){
std::cout << "Enter the amount of angles and point coordinates (x and y) via space button: ";
int N{readAmountOfAngles()};
Point dot{readPointCoordinates()};
int coordinates[N-1];
for (int i{0};i<=2*N-1;++i){
    std::cin >> coordinates[i];
}
int counter{0};
for(int p{0}, z{1};(p<N);++p, ++z){
    if (2*N > 2*z +1){
        if (ifCollide(dot ,coordinates[2*p], coordinates[2*p+1], coordinates[2*z], coordinates[2*z+1])== true)
        ++counter;
    }
    else{
        if (ifCollide(dot, coordinates[2*p], coordinates[2*p+1], coordinates[0], coordinates[1])==true)
        ++counter;
    }
}
std::cout << counter<<"\n";
if (counter%2 == 0)
    std::cout << "NO";
else
    std::cout << "YES";
return 0;
}
